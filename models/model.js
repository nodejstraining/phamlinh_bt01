
var mongoose = require('mongoose');

try {
    mongoose.connect('mongodb://localhost/ex'); //- starting a db connection
}catch(err) {
    mongoose.createConnection('mongodb://localhost/ex'); //- starting another db connection
}

var Schema = mongoose.Schema;

var user = new Schema({
	name: {type: String, required: true},
	email: {type: String},
	password: {type: String},
	created_at: Date,
 	updated_at: Date
});

user.pre('save', function (next){
  var currentDate = new Date().toISOString();
  this.updated_at = currentDate;
  if(!this.created_at){
    this.created_at = currentDate;
  }
  next();
});

// the schema is useless so far
// we need to create a model using it
var User = mongoose.model('User', user);
// make this available to our users in our Node applications
module.exports = {
  User     : User
}


